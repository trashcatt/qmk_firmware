```
 __                        __                      __      
/\ \__                    /\ \                    /\ \__   
\ \ ,_\ _ __   __      ___\ \ \___     ___     __ \ \ ,_\  
 \ \ \//\`'__/'__`\   /',__\ \  _ `\  /'___\ /'__`\\ \ \/  
  \ \ \\ \ \/\ \L\.\_/\__, `\ \ \ \ \/\ \__//\ \L\.\\ \ \_ 
   \ \__\ \_\ \__/.\_\/\____/\ \_\ \_\ \____\ \__/.\_\ \__\
    \/__/\/_/\/__/\/_/\/___/  \/_/\/_/\/____/\/__/\/_/\/__/
                                          -- my Koyu Layout
```

### Layer 0: Base Layer

![baselayer](https://blog.trashcat.xyz/photos/Koyubase.png)

My base layer is Dvorak with a few personal QOL changes like Space Cadet shift, swapped Caps and Ctrl and Ctrl/Esc for Vim.



### Layer 1: Gaming/QWERTY Layer

![game](https://blog.trashcat.xyz/photos/Koyuqwerty.png)

A basic QWERTY Layer so normies can use my keyboard. It also makes gaming easier in some cases. Mostly Escape From Tarkov.



### Layer 2: Function Layer

![fn](https://blog.trashcat.xyz/photos/KoyuFN.png)

This is my function layer. I only recently learned that if I make my FN layer after my "Gaming" layer, I only need one FN layer and it'll work on all layers below it. I'm an idiot sometimes.

### Todo
- [x] Update Readme to show Koyu images instead of Whitefox images
- [ ] Not sure yet...
