# Trashcat's Tada68

Here is my tada68 layout. I did an entire write up of it over on my [blog](https://blog.trashcat.xyz/2020/01/my-custom-keyboard-layout-tada68/).

# Base Layer

![](https://blog.trashcat.xyz/photos/baselayer.png)



# Function Layer

![img](https://blog.trashcat.xyz/photos/fnlayer.png)



# Num Pad Layer

![](https://blog.trashcat.xyz/photos/numlayer.png)