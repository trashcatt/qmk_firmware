```
 __                        __                      __      
/\ \__                    /\ \                    /\ \__   
\ \ ,_\ _ __   __      ___\ \ \___     ___     __ \ \ ,_\  
 \ \ \//\`'__/'__`\   /',__\ \  _ `\  /'___\ /'__`\\ \ \/  
  \ \ \\ \ \/\ \L\.\_/\__, `\ \ \ \ \/\ \__//\ \L\.\\ \ \_ 
   \ \__\ \_\ \__/.\_\/\____/\ \_\ \_\ \____\ \__/.\_\ \__\
    \/__/\/_/\/__/\/_/\/___/  \/_/\/_/\/____/\/__/\/_/\/__/
                                          -- my XD60 Layout
```

### Layer 0: Base Layer

![baselayer](https://blog.trashcat.xyz/photos/xd60Base.png)

My base layer is Dvorak with a few personal QOL changes like Space Cadet shift, swapped Caps and Ctrl and Ctrl/Esc for Vim.



### Layer 1: Gaming/QWERTY Layer

![game](https://blog.trashcat.xyz/photos/xd60QWERTY.png)

A basic QWERTY Layer so normies can use my keyboard. It also makes gaming easier in some cases. Mostly Escape From Tarkov. I made the bottom right keys Page Up and Page Down as those are used for zeroing the sights. I also removed that pesky Windows key from this layout so I don't tab myself out of games.



### Layer 2: Function Layer

![fn](https://blog.trashcat.xyz/photos/xd60Fn.png)

The function layer. It took me **WAAAAAYYYYY** too long to realize that I could put the Function layer below the other two layers to only have to use one FN layer. I was acutally making multiple FN layers in the past like a dummy.

### Layer 3: Number Pad Layer

![numpad](https://blog.trashcat.xyz/photos/xd60Num.png)

A litte num pad layer to add a bit of QOL. If you hold the ESC key on the base layer it becomes a num pad layer on the above keys. I neat little trick I picked up from my friend [hund](https://hund.tty1.se/2019/06/23/this-is-my-custom-keyboard-layout.html). It's also a thing on a lot of laptop keyboards but whatever. LUL


### To Build
To build this keymap simply use this command:  
`make xd60/rev2:trashcatt:dfu`
