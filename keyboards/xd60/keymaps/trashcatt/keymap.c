#include QMK_KEYBOARD_H

#define NM_ESC LT(M(3), KC_ESC)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  // 0: Base Layer
  LAYOUT_all(
      NM_ESC,  KC_1,    KC_2,    KC_3,    KC_4,   KC_5,   KC_6,   KC_7,   KC_8,   KC_9,    KC_0,    KC_LBRC,  KC_RBRC,  KC_GRV,  KC_BSLS,    \
      KC_TAB,  KC_QUOT,    KC_COMM,    KC_DOT,    KC_P,   KC_Y,   KC_F,   KC_G,   KC_C,   KC_R,    KC_L,    KC_SLSH,  KC_EQL,           KC_BSPC,   \
      LCTL_T(KC_ESC), KC_A,    KC_O,    KC_E,    KC_U,   KC_I,   KC_D,   KC_H,   KC_T,   KC_N,    KC_S, KC_MINS,  KC_NO,             KC_ENT,    \
      KC_LSPO, KC_NO,   KC_SCLN,    KC_Q,    KC_J,   KC_K,   KC_X,   KC_B,   KC_M,   KC_W, KC_V,  KC_Z,  KC_NO, KC_RSPC,    MO(2),      \
      KC_CAPS, KC_LGUI, KC_LALT,                          KC_SPC,                          KC_RALT, KC_RGUI,    KC_NO, KC_APP,  KC_RCTL),

  // 1: QWERTY/GAMING Layer
  LAYOUT_all(
      KC_ESC,   KC_1,   KC_2,   KC_3,   KC_4,  KC_5,  KC_6,  KC_7,  KC_8,  KC_9,   KC_0,  KC_MINS,   KC_EQL,  KC_GRV,   KC_BSLS,    \
      KC_TAB,   KC_Q, KC_W,   KC_E, KC_R,KC_T,KC_Y,KC_U,  KC_I, KC_O,   KC_P, KC_LBRC,  KC_RBRC,           KC_BSPC,    \
      KC_CAPS,   KC_A, KC_S, KC_D,KC_F, KC_G, KC_H,KC_J,  KC_K,  KC_L,   KC_SCLN, KC_QUOT,  KC_NO,             KC_ENT,    \
      KC_LSFT, KC_NO,   KC_Z,   KC_X,  KC_C,KC_V,  KC_B,  KC_N,KC_M,KC_COMM, KC_DOT,  KC_SLSH,  KC_NO, KC_RSFT,  MO(2),      \
      KC_LCTL, KC_NO, KC_LALT,                          KC_SPC,                          KC_RALT, KC_NO,    KC_NO, KC_PGUP,KC_PGDN),

  // 2: Function Layer
  LAYOUT_all(
      RESET,   KC_F1,   KC_F2,   KC_F3,   KC_F4,  KC_F5,  KC_F6,  KC_F7,  KC_F8,  KC_F9,   KC_F10,  KC_F11,   KC_F12,  KC_F13,   TG(1),    \
      KC_NO,   KC_BTN1, KC_MS_U,   KC_BTN2, KC_NO,KC_NO,KC_NO,KC_NO,  KC_NO, KC_PSCR,   KC_HOME, KC_UP,  KC_END,           KC_DEL,    \
      KC_NO,   KC_MS_L, KC_MS_D, KC_MS_R,RGB_VAI, RGB_VAD, KC_NO,KC_NO,  KC_NO,  KC_NO,   KC_LEFT, KC_RIGHT,  KC_NO,             KC_NO,    \
      KC_TRNS, KC_NO,   RGB_TOG,   RGB_MOD,  RGB_RMOD,RGB_HUI,  RGB_HUD,  RGB_SAI,RGB_SAD,KC_NO, KC_PGUP,  KC_DOWN,  KC_PGDN, KC_NO,  KC_NO,      \
      KC_LCTL, KC_NO, KC_LALT,                         KC_TRNS,                          KC_NO, KC_NO,    KC_MPRV, KC_MPLY,KC_MNXT),

  // 3: Numpad Layer
  LAYOUT_all(
      KC_TRNS,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,   KC_NO,  KC_NO,   KC_NO,  KC_NO,   KC_NO,    \
      KC_NLCK,   KC_NO, KC_NO,   KC_NO, KC_NO,KC_NO,KC_P7,KC_P8,  KC_P9, KC_NO,   KC_NO, KC_NO,  KC_NO,           KC_NO,    \
      KC_NO,   KC_NO, KC_NO, KC_NO,KC_NO, KC_NO, KC_P4,KC_P5,  KC_P6,  KC_NO,   KC_NO, KC_NO,  KC_NO,             KC_ENT,    \
      KC_NO, KC_NO,   KC_NO,   KC_NO,  KC_NO,KC_NO,  KC_NO,  KC_P1,KC_P2,KC_P3, KC_NO,  KC_NO,  KC_NO, KC_NO,  KC_NO,      \
      KC_NO, KC_NO, KC_NO,                          KC_P0,                          KC_NO, KC_NO,    KC_NO, KC_NO,KC_NO),



};

// Loop
void matrix_scan_user(void) {
  // Empty
};
